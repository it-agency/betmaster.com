'use strict';

// Инициализируем плагины
var gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    //csso = require('gulp-csso'), // Минификация CSS
    autoprefixer = require('gulp-autoprefixer'),
    server = require('browser-sync'), //Веб сервер
    //rename = require("gulp-rename"),
    csscomb = require('gulp-csscomb'),
    //uglify = require('gulp-uglify'),
    //imagemin = require('gulp-imagemin'),
    typograf = require('gulp-typograf'),
    concat = require('gulp-concat'); // Склейка файлов

var stls = [
        './dist/stylus/base/*.styl',
        './dist/stylus/layout/*.styl',
        './dist/stylus/module/*.styl',
        './dist/stylus/state/*.styl'
    ];

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src(stls)
        .pipe(stylus()) // собираем stylus
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(csscomb())
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(server.stream()); // записываем css
});

// Собираем html из Jade
gulp.task('jade', function() {
    gulp.src(['./dist/template/**/*.jade', '!./dist/template/**/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./dist/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(typograf({
            lang: 'ru',
            mode: 'name',
            disable: ['ru/optalign/*'], // disable rules
            enable: ['ru/money/ruble'] // enable rules
        }))
        .pipe(gulp.dest('./public/')) // Записываем собранные файлы
        .pipe(server.stream());
});

// Запуск сервера разработки gulp watch
gulp.task('watch',['stylus'], function() {

    server.init({
        server: {
            baseDir: "./public"
        }
    });

    gulp.on('error', console.log);
    // Предварительная сборка проекта
    gulp.watch('dist/stylus/**/*.styl', ['stylus']);
});

//Собираем билд
gulp.task('start', function() {

    gulp.src(['./dist/template/*.jade', '!./dist/template/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./dist/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(typograf({
            lang: 'ru',
            mode: 'name',
            disable: ['ru/optalign/*'], // disable rules
            enable: ['ru/money/ruble'] // enable rules
        }))
        .pipe(gulp.dest('./public/')) // Записываем собранные файлы
        .pipe(server.stream());

    gulp.src(stls)
        .pipe(stylus()) // собираем stylus
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(csscomb())
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(server.stream()); // записываем css

});

gulp.task('default', ['watch']);