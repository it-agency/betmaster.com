<?php
/**
 * Created by PhpStorm.
 * User: maximburanbaev
 * Date: 4/5/16
 * Time: 2:28 PM
 */
ini_set('display_errors','On');
error_reporting('E_ALL');

require 'phpMailer.php';
require 'class.smtp.php';
require 'sb.php';

function regUser($email, $password, $confirm_password, $type, $actionpay = NULL) {
    $res = "";

    $sb = new SourceBuster();
    $current = $sb->getData($_COOKIE['sbjs_current']);
    $data = array(
        'email' => $email,
        'password' => $password,
        'password_confirm' => $confirm_password,
        'marketing' => array(
            "utm_source" => $current["src"],
            "utm_medium" => $current["mdm"],
            "utm_campaign" => $current["cmp"],
            "utm_content" => $type . $current["cnt"]
        )
    );

    if ($actionpay){
        $data['marketing']['actionpay'] = $actionpay;
    }

    $data_string = json_encode($data);
    
    //Dev url https://betmaster-preprod.kindbeetle.ru:443/api/auth/register
    
    $ch = curl_init('https://betmaster.com/api/auth/register');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );

    $res .= curl_exec($ch);
    return $res;    
}

function sendMail($email, $name, $message) {
    $email = strip_tags($email);
    $name = strip_tags($name);
    $message = strip_tags($message);

    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'bot@izst.ru';                 // SMTP username
    $mail->Password = 'a23zxaqw';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    $mail->setFrom('bot@izst.ru', 'Mailer');
    $mail->addAddress('birzhev@betmaster.com');     // Add a recipient     // Add a recipient
    $mail->addAddress('pavel@it-agency.ru');     // Add a recipient
    //$mail->addAddress('ip@it-agency.ru');     // Add a recipient

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = '=?UTF-8?B?'.base64_encode('Вопрос с сайте promo.betmaster.com').'?=';
    $mail->Body    = "
    <div style=\"font-family: 'PT Sans', Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1.4; display: block; color: #333; background: #e4f1f7; padding: 40px 60px;\" bgcolor=\"#e4f1f7\">
        <table style=\"font-family: 'PT Sans', Arial, Helvetica, sans-serif; -webkit-border-radius: 3px; border-radius: 3px; padding: 20px; border: 1px dashed #63859e; font-size: 16px; line-height: 1.4; display: block; color: #333;\"'>
            <tr>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; font-weight: bold; width: 150px;\" valign=\"top\">Имя:</td>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; width: 400px;\" valign=\"top\">$name</td>
            </tr>
            <tr>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; font-weight: bold; width: 150px;\" valign=\"top\">Email:</td>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; width: 400px;\" valign=\"top\">$email</td>
            </tr>
            <tr>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; font-weight: bold; width: 150px;\" valign=\"top\">Сообщение:</td>
                <td style=\"padding-top: 10px; padding-bottom: 10px; vertical-align: top; font-size: 16px; width: 400px;\" valign=\"top\">$message</td>
            </tr>
        </table>
    </div>
    ";
    $res = [];
    if(!$mail->send()) {
        $res['status']= 500;
        $res['Error']= $mail->ErrorInfo;

    } else {
        $res['status']= 200;
    }
    return json_encode($res);
}


/* Handler */
$res = "";

if(isset($_POST["action"])) {
    switch($_POST["action"]) {
        case "reg":
            $res .= regUser($_POST["email"], $_POST["password"], $_POST["password"], $_POST["type"], $_POST['actionpay']);
            break;
        case "sendMail":
            $res .= sendMail($_POST["email"], $_POST["name"], $_POST["message"]);
            break;
        default:
            $res .= "Undefined Action";
            break;
    }
} else {
    $res .= "Error! No action!";
}

echo($res);




