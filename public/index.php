<?php
ini_set('display_errors','On');
error_reporting('E_ALL');

/* Логика сплит теста */

function setSplit($trigger) {
    if($trigger) {
        setcookie("formReg", "new1");
        return true;
    } else {
        setcookie("formReg", "old");
        return false;
    }
}

if(isset($_GET["formReg"])) {
    if ($_GET["formReg"] == "new1") {
        $splitForm = setSplit(true);
    } elseif ($_GET["formReg"] == "old") {
        $splitForm = setSplit(false);
    }
} else {
    if(isset($_COOKIE["formReg"])) {
        if($_COOKIE["formReg"] == "new1") {
            $splitForm = setSplit(true);
        } elseif($_COOKIE["formReg"] == "old") {
            $splitForm = setSplit(false);
        }
    } else {
        $splitForm = setSplit(rand(0,1));
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Betmaster.com: Ставки на&nbsp;футбол</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280, user-scalable=no">
    <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link href="/css/main.css" type="text/css" rel="stylesheet">
    <link href="/css/swipe-slider.css" type="text/css" rel="stylesheet">

</head>
<body>
<div class="l-home">
    <div class="l-wrapper">
        <div class="l-head">
            <div class="logo"><a href="/"><img src="/img/logo.png"><span>Ставки на&nbsp;футбол</span></a></div>
            <div class="sign-in"><a href="#" data-toggle="modal" data-target=".js-modal-reg" class="js-link-reg">Регистрация</a><a href="https://betmaster.com/signin" class="js-link-sign">Войти</a></div>
        </div>
        <div class="l-calc">
            <div class="calc">
                <div class="calc-head">
                    <h1 class="calc-title">Бонус 50 % на&nbsp;каждый депозит</h1>
                    <div class="calc-deposit">
                        <ul class="deposit-list js-deposit-list">
                            <li data-position="100" data-state="default" data-price="100" style="opacity:0.2;" class="deposit-item js-deposit-item"><a href="#">100</a></li>
                            <li data-position="80" data-state="default" data-price="200" style="opacity:0.3;" class="deposit-item js-deposit-item"><a href="#">200</a></li>
                            <li data-position="60" data-state="default" data-price="300" style="opacity:0.4;" class="deposit-item js-deposit-item"><a href="#">300</a></li>
                            <li data-position="40" data-state="default" data-price="400" style="opacity:0.5;" class="deposit-item js-deposit-item"><a href="#">400</a></li>
                            <li data-position="20" data-state="default" data-price="500" style="opacity:0.6;" class="deposit-item js-deposit-item"><a href="#">500</a></li>
                            <li data-position="0" data-state="active" data-price="1000" style="opacity:0.7;" class="deposit-item js-deposit-item"><a href="#">1000</a></li>
                            <li data-position="-20" data-state="default" data-price="2000" style="opacity:0.8;" class="deposit-item js-deposit-item"><a href="#">2000</a></li>
                            <li data-position="-40" data-state="default" data-price="3000" style="opacity:1;" class="deposit-item js-deposit-item"><a href="#">3000</a></li>
                            <li data-position="-60" data-state="default" data-price="4000" style="opacity:0.8;" class="deposit-item js-deposit-item"><a href="#">4000</a></li>
                            <li data-position="-80" data-state="default" data-price="5000" style="opacity:0.7;" class="deposit-item js-deposit-item"><a href="#">5000</a></li>
                            <li data-position="-100" data-state="default" data-price="10000" style="opacity:0.6;" class="deposit-item js-deposit-item"><a href="#">10 000</a></li>
                            <li data-position="-120" data-state="default" data-price="20000" style="opacity:0.5;" class="deposit-item js-deposit-item"><a href="#">20 000</a></li>
                            <li data-position="-140" data-state="default" data-price="30000" style="opacity:0.4;" class="deposit-item js-deposit-item"><a href="#">30 000</a></li>
                            <li data-position="-160" data-state="default" data-price="40000" style="opacity:0.3;" class="deposit-item js-deposit-item"><a href="#">40 000</a></li>
                            <li data-position="-180" data-state="default" data-price="50000" style="opacity:0.2;" class="deposit-item js-deposit-item"><a href="#">50 000</a></li>
                        </ul><span class="deposit-title">Внеси <span class="deposit-price js-curr-price">1000</span><span class="deposit-ruble i-ruble"> q</span></span>
                    </div>
                    <div class="arrow"><span>подарим</span><img src="/img/arrow.png"></div>
                    <div class="calc-bonus"><span class="calc-bonus-cash">+ <span class="js-cash">500</span><i class="i-ruble"> q</i></span><span class="calc-bonus-deposit"><span class="js-deposit">1000</span><i class="i-ruble"> q</i></span><img src="/img/bonus.png"></div>
                    <div class="arrow arrow-right"><span>доступно</span><img src="/img/arrow.png"></div>
                    <div class="calc-total"><span class="calc-total-price"><span class="js-total">1500</span><i class="i-ruble"> q</i></span>
                        <div class="calc-total-desc calc-total-desc-infinity"><img src="/img/infinity.png"><span>следующие депозиты &mdash;<br>снова +  50 %</span></div>
                        <button id="btnHome" href="#" data-toggle="modal" data-target=".js-modal-reg" class="btn"><span><i class="b-after"></i>Зарегистрироваться<i class="b-before"></i></span></button>
                        <div class="calc-total-desc calc-total-desc-btn">Чтобы получить бонус</div>
                    </div>
                </div>
                <div class="calc-foot">
                    <div class="calc-foot-rubls">
                        <div>Минимальная сумма<br>ввода и&nbsp;вывода&nbsp;&mdash; 50<i class="i-ruble"> q</i></div><img src="/img/rubls.png">
                    </div>
                    <div class="calc-foot-cards">
                        <div>Автоматические выплаты на&nbsp;Visa<br>и&nbsp;MasterCard</div><img src="/img/cards.png">
                    </div>
                    <div class="calc-foot-surprize">
                        <div>Максимальный бонус<br>25 000<i class="i-ruble"> q</i></div><img src="/img/surprize.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="l-modal-curtain js-modal-curtain js-modal-reg">
    <div class="modal modal-reg-new <?php if($splitForm){ echo 'is-modal-bg-two';}?>">
        <span data-dismiss="modal" class="modal-close"></span>
        <div class="modal-title"><?php if($splitForm){ echo 'Зарегистрироваться';} else {echo 'Регистрация';}?></div>

        <form name="modal_reg" method="POST" class="modal-form js-form-reg ">
            <input type="hidden" name="action" value="reg">
            <input type="hidden" name="type" value="version1_">

            <?php
            if(isset($splitForm)){
                echo '<input type="hidden" name="actionpay" value='  .$_GET["actionpay"].  '>';
            }
            ?>
            <div class="modal-group">
                <input name="email" placeholder="Электронная почта" type="text" class="modal-input modal-reg-email js-email">
                <span class="modal-group-hint js-email-hint">Превосходно!</span>
            </div>
            <div class="modal-group">
                <div class="modal-input-wrap">
                    <input name="password" id="form-password" placeholder="Пароль" type="password" class="modal-input modal-reg-password js-password">
                    <span class="modal-input-length js-password-length"></span>
                </div>
                <span class="modal-group-hint js-password-hint">Без пароля вообще никак :-(</span>
            </div>
            <div class="modal-group">

                <?php if($splitForm){?>

                    <button class="btn btn-modal">
              <span>
                <i class="b-after"></i>
                Отправить
                <i class="b-before"></i>
              </span>
                    </button>

                <?php } else {?>

                    <button class="btn-white">
                        <span>Отправить</span>
                    </button>


                <?php } ?>

                <span class="modal-msg js-modal-msg">
              <span>Спасибо за&nbsp;вопрос</span>
            </span>

                <div class="modal-arrow js-modal-arrow "></div>

            </div>
        </form>


        <div class="modal-success">
            <div class="modal-success-img"><img src="/img/reg-success.png" width="100%"></div>
            <div class="modal-success-title">Проверьте почту</div>
            <div class="modal-success-desc">На&nbsp;вашу почту придет письмо с&nbsp;ссылкой<br>для подтверждения аккаунта</div>
            <div class="modal-success-actionpay"></div>
        </div>

        <div class="modal-bonus">
            <div class="modal-bonus-one">
                <span class="modal-bonus-img"><img src="/img/ball.png" alt="Ставка от 10 рублей"></span>
                <span class="modal-bonus-title">Ставка от 10 рублей</span>
            </div>
            <div class="modal-bonus-two">
                <span class="modal-bonus-img"><img src="/img/ball.png" alt="Выигрыш по бонусу сразу доступен к выводу"></span>
                <span class="modal-bonus-title">Выигрыш по бонусу <br> сразу доступен к выводу</span>
            </div>
            <div class="modal-bonus-three">
                <span class="modal-bonus-img"><img src="/img/ball.png" alt="Автоматический вывод средств"></span>
                <span class="modal-bonus-title">Автоматический вывод средств</span>
            </div>
        </div>

    </div>
</div>
<script src="/js/jquery-1.12.2.min.js" rel="text/javascript"></script>
<script src="/js/modal.js" rel="text/javascript"></script>
<script src="/js/sourcebuster.min.js" rel="text/javascript"></script>
<script src="/js/jquery.activity.min.js" rel="text/javascript"></script>
<script src="/js/jquery.placeholder.js" rel="text/javascript"></script>
<script src="/js/common.js" rel="text/javascript"></script>
<script src="/js/jquery.textchange.js"></script>

<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-76176613-1', 'auto');
    ga('send', 'pageview');

    <?php if($splitForm) { ?>
        ga('set', 'FormReg', 'New1');
    <?php } else { ?>
        ga('set', 'FormReg', 'Old');
    <?php } ?>

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter36659430 = new Ya.Metrika({id:36659430,
                    webvisor:true,
                    clickmap:true,
                    trackHash:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/36659430" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>