$(function() {
    /* Кастомная валидация пароля */
    $("#form-password").bind("textchange", function(){
        if (this.value.length < 8) {
            this.setCustomValidity('Длина пароля — от 8 знаков');
        } else {
            this.setCustomValidity('');
        }
    });

    /*Отправка форм*/
    $('form').on('submit', function(e){
        e.preventDefault();
        var form = $(this),
            modal = form.closest('.modal'),
            curtain = form.closest('.js-modal-curtain'),
            formMsg = form.find('.js-modal-msg').removeClass('.is-modal-msg-visible');
            //console.log(form.serialize());

        /* Если это отправка вопрос, кнопку отправки делаем сразу неактивной */
        if(form.attr('name') == 'modal_question') {
            $(".js-send-question").attr("disabled","disabled");
        }

        $.post( "/ajaxHandler.php", form.serialize())
            .done(function( data ) {

                //Конвертируем данные в объект
                console.log(data);
                data = JSON.parse(data);
                console.log(data);

                //Форма регистрации
                if(form.attr('name') =='modal_reg'){
                    //Если есть код ошибки
                    if (typeof(data.code) != "undefined" && data.code !== null){
                        formMsg.addClass('is-modal-msg-visible').find('span').text(data.message.ru);
                    }else {

                        //Иначе если есть токен авторизации
                        if (typeof(data.notifier.auth_token) != "undefined" && data.notifier.auth_token !== null){

                            /*Успешная отправка формы регистации*/
                            ga('send', 'event', 'reg_form', 'sent');

                            //Показываем сообщение об успешной регистрации
                            modal.addClass('is-modal-success');
                            setTimeout(function(){
                                curtain.removeClass('in').css({'display':'none'});
                                modal.removeClass('is-modal-success');
                            }, 10000);
                        }
                        else {
                            formMsg.addClass('is-modal-msg-visible').find('span').text('Извините произошла неизвестная ошибка :(');
                        }
                    }

                }
                if(form.attr('name') =='modal_question'){

                    if (typeof(data.status) != "undefined" && data.status !== null ){

                        if (data.status === 200){
                            /*Успешная отправка вопроса*/
                            ga('send', 'event', 'faq_form', 'sent');

                            formMsg.addClass('is-modal-msg-visible').find('span').text('Сообщение успешно отправлено!');
                            $(".js-send-question").attr("disabled","disabled");  //На всякий пожарный
                        } else{
                            formMsg.addClass('is-modal-msg-visible').find('span').text(data.status+":"+data.Error);
                        }

                    } else {
                        formMsg.addClass('is-modal-msg-visible').find('span').text('Извините произошла неизвестная ошибка :(');
                    }
                }

            });
    });

});