;(function(){

    var SETTINGS = {
        VALIDATION_CONF:{
            MIN_PWD_LENGTH:8
        },
        HINT:{
            ERROR:{
                PWD_EMPTY:'Пароль не менее 8 символов :-(',
                EML_EMPTY:'Не похоже на почту :-(',
                EML_INVALID:'Не похоже на почту :-(',
                EML_BUSY:'Уже используется :-(',
                OLD:'Попробуйте ещё раз, не сработало :\'‑('
            },
            SUCCESS:{
                EML:'Превосходно!',
                PWD:'Пароль что надо!'
            },
            INFO:{
                PWD:function(count){
                    var res = '';

                    switch (count) {
                        case 5:
                            res = "Чтобы не взломали — ещё "+ count +" знаков";
                            break;
                        case 1:
                            res = "Чтобы не взломали — ещё "+ count +" знак";
                            break;
                        default:
                            res = "Чтобы не взломали — ещё "+ count +" знака";
                            break;
                    }

                    return res;
                }
            }
        },
        ELEM:{
            $PWD_HINT : $('.js-password-hint'),
            $EML_HINT : $('.js-email-hint'),
            $PWD : $('.js-password'),
            $EML : $('.js-email'),
            $PWD_LENGTH : $('.js-password-length'),
            $ARROW : $('.js-modal-arrow'),
            $FORM_REG : $('.js-form-reg')
        }
    };

    $(document).on('mousemove', function (event) {
        event.preventDefault();
        btnOpacityByOffset($('#btnHome .b-after'),$('#btnHome .b-before'));
    });

    function btnOpacityByOffset($after, $before){
        var btnI = $after;
        var btnB = $before;
        o = btnI.offset();
        w = btnI.width();
        h = btnI.height();
        x1 = o.left + w/2;
        y1 = o.top + h/2;
        x2 = event.pageX;
        y2 = event.pageY;
        l = Math.pow ((Math.pow(x2-x1,2) + Math.pow(y2-y1, 2)),.5);
        l = Math.max (Math.min(l,600),100);
        l = (l-100)/500;
        btnI.css('opacity', 0.3 + (1-l) * 0.5);
        if(l < 0.3){
            l = 0;
        }
        btnB.css('opacity', l);
    }

    $(document).ready(function(){

        var is_eml_valid = false;
        var is_pwd_valid = false;

        $('.js-deposit-item').on('click', function(e){

            e.preventDefault();

            ga('send', 'event', 'calc_actions', 'deposit');

            var elem = $(this);
            var $list = $('.js-deposit-list');
            var $listItems = $list.find('li');
            var offsetTop = elem.attr('data-position')+'px';
            var deposit = parseInt(elem.attr('data-price'));
            var cash = 0.5;
            var total = deposit+(deposit*cash);
            var $currPrice = $('.js-curr-price');

            $list.animate({"top":offsetTop}, {
                    duration: 100,
                    easing: 'linear',
                    step: function (now, fx) {
                        $listItems.each(function() {
                            var curPos = parseInt($(this).data('position'));
                            var fNow = Math.floor(now);
                            var price = $(this).data('price');

                            if (fNow >= curPos - 15 && fNow < curPos + 15) {
                                $(this).attr('data-state', 'active');
                                $currPrice.html(priceFormat(price.toString()));
                                $(this).nextAll('li').each(function (index, item) {
                                    switch (index+1) {
                                        case 2:
                                            $(item).data('opacity', 0.7);
                                            break;
                                        case 3:
                                            $(item).css('opacity', 0.6);
                                            break;
                                        case 4:
                                            $(item).css('opacity', 0.5);
                                            break;
                                        case 5:
                                            $(item).css('opacity', 0.4);
                                            break;
                                        default:
                                            $(item).css('opacity', 1);

                                    }
                                });
                                $(this).prevAll('li').each(function (index, item) {
                                    switch (index) {
                                        case 2:
                                            $(item).css('opacity', 0.7);
                                            break;
                                        case 3:
                                            $(item).css('opacity', 0.6);
                                            break;
                                        case 4:
                                            $(item).css('opacity', 0.5);
                                            break;
                                        case 5:
                                            $(item).css('opacity', 0.4);
                                            break;
                                        default:
                                            $(item).css('opacity', 1);
                                    }
                                })
                            }
                            else {
                                $(this).attr('data-state', 'default');
                            }
                        })
                    },
                    done: function () {
                        if (deposit == deposit){
                            $('.js-cash').html(priceFormat(deposit*cash));
                            $('.js-deposit').html(priceFormat(deposit));
                            $('.js-total').html(priceFormat(total.toString()));
                        }
                    }
                }
            );
            //$('.deposit-item[data-state="active"]').attr('data-state','default');
        });

        SETTINGS.ELEM.$PWD.on('keyup', function(){
            is_pwd_valid = validationPwd($(this));
            /*Генерим событие*/
            SETTINGS.ELEM.$FORM_REG.trigger('cred.change');
        });

        SETTINGS.ELEM.$EML.on('change', function(){
            is_eml_valid = validationEml($(this));
            /*Генерим событие*/
            SETTINGS.ELEM.$FORM_REG.trigger('cred.change');
        });

        /*Отлавливаем событие*/
        SETTINGS.ELEM.$FORM_REG.on('cred.change', function(){
            if (is_eml_valid && is_pwd_valid){
                SETTINGS.ELEM.$ARROW.addClass('is-modal-arrow-visible');
            } else {
                SETTINGS.ELEM.$ARROW.removeClass('is-modal-arrow-visible');
            }
        });

        function validationPwd($field){
            var val = $field.val();
            var l = val.length;
            var delta = 8 - l;
            var pwd_length = l*12.5;

            SETTINGS.ELEM.$PWD_LENGTH.css('width', pwd_length + '%');
            SETTINGS.ELEM.$PWD_HINT.removeClass('is-group-hint-success is-group-hint-info is-group-hint-error');

            if (l >= SETTINGS.VALIDATION_CONF.MIN_PWD_LENGTH){
                SETTINGS.ELEM.$PWD_HINT.text(SETTINGS.HINT.SUCCESS.PWD).addClass('is-group-hint-success');
                return true;
            }
            else {
                if ( l > 2){
                    SETTINGS.ELEM.$PWD_LENGTH.addClass('is-input-length-visible');
                    SETTINGS.ELEM.$PWD_HINT.text(SETTINGS.HINT.INFO.PWD(delta)).addClass('is-group-hint-info');
                } else {
                    SETTINGS.ELEM.$PWD_LENGTH.removeClass('is-input-length-visible');
                }

                if( l === 0){
                    SETTINGS.ELEM.$PWD_HINT.text(SETTINGS.HINT.ERROR.PWD_EMPTY).addClass('is-group-hint-error');
                }
            }
            return false;
        }

        function validationEml($field){
            var val = $field.val();
            var l = val.length;
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

            SETTINGS.ELEM.$EML_HINT.removeClass('is-group-hint-success is-group-hint-info is-group-hint-error');

            if (pattern.test(val)){
                SETTINGS.ELEM.$EML_HINT.text(SETTINGS.HINT.SUCCESS.EML).addClass('is-group-hint-success');
                return true;
            }
            else {
                if (l !== 0){
                    SETTINGS.ELEM.$EML_HINT.text(SETTINGS.HINT.ERROR.EML_INVALID).addClass('is-group-hint-error');
                }
                else {
                    SETTINGS.ELEM.$EML_HINT.text(SETTINGS.HINT.ERROR.EML_EMPTY).addClass('is-group-hint-error');
                }
            }
            return false;
        }

        /*Отправка форм*/
        $('form').on('submit', function(e){

            e.preventDefault();

            var $form = $(this);

            console.log($form.attr('name'));

            if ($form.attr('name') == 'modal_reg'){

                is_eml_valid = validationEml(SETTINGS.ELEM.$EML);
                is_pwd_valid = validationPwd(SETTINGS.ELEM.$PWD);

                if (is_eml_valid && is_pwd_valid){
                    formOnSubmit($form);
                }
            }

        });

        function formOnSubmit($form){
            var $modal = $form.closest('.modal');
            var $curtain = $form.closest('.js-modal-curtain');
            var $formMsg = $form.find('.js-modal-msg');

            $formMsg.removeClass('.is-modal-msg-visible');

            $.post( "/dev/ajaxHandler.php", $form.serialize())
                .done(function( data ) {

                    console.log(data);

                    //Конвертируем данные в объект
                    data = JSON.parse(data);

                    //Форма регистрации
                    if($form.attr('name') =='modal_reg'){

                        SETTINGS.ELEM.$EML_HINT.removeClass('is-group-hint-success is-group-hint-info is-group-hint-error');
                        SETTINGS.ELEM.$PWD_HINT.removeClass('is-group-hint-success is-group-hint-info is-group-hint-error');
                        SETTINGS.ELEM.$PWD_LENGTH.removeClass('is-input-length-visible');
                        SETTINGS.ELEM.$ARROW.removeClass('is-modal-arrow-visible');

                        //Если есть код ошибки
                        if (typeof(data.code) != "undefined" && data.code !== null){
                            //$formMsg.addClass('is-modal-msg-visible').find('span').text(data.message.ru);
                            if(data.code === 402){
                                SETTINGS.ELEM.$EML_HINT.text(SETTINGS.HINT.ERROR.EML_BUSY).addClass('is-group-hint-error');
                            }
                            else {
                                $formMsg.addClass('is-modal-msg-visible').find('span').text(SETTINGS.HINT.ERROR.OLD);
                            }

                        }else {
                            SETTINGS.ELEM.$EML_HINT.removeClass('is-group-hint-error');

                            //Иначе если есть токен авторизации
                            if (typeof(data.notifier.auth_token) != "undefined" && data.notifier.auth_token !== null){

                                /*Успешная отправка формы регистации*/
                                ga('send', 'event', 'reg_form', 'sent');

                                //Показываем сообщение об успешной регистрации
                                $modal.addClass('is-modal-success');

                                if ($('input[name="actionpay"]').length){

                                    var user_id = data.id;
                                    var actionpay = $('input[name="actionpay"]').val();
                                    var actionpayHtml = '<script> window.APRT_DATA = {pageType: 6};</script><img src="//apypxl.com/ok/9776.png?actionpay='+actionpay+'&apid='+user_id+'" height="1" width="1" />';

                                    $('.modal-success-actionpay').html(actionpayHtml);

                                }

                                setTimeout(function(){
                                    $curtain.removeClass('in').css({'display':'none'});
                                    $modal.removeClass('is-modal-success');
                                }, 10000);
                            }
                            else {
                                $formMsg.addClass('is-modal-msg-visible').find('span').text(SETTINGS.HINT.ERROR.OLD);
                            }
                        }

                    }
                    if($form.attr('name') =='modal_question'){

                        if (typeof(data.status) != "undefined" && data.status !== null ){

                            if (data.status === 200){
                                /*Успешная отправка вопроса*/
                                ga('send', 'event', 'faq_form', 'sent');

                                $formMsg.addClass('is-modal-msg-visible').find('span').text('Сообщение успешно отправлено!');
                                $(".js-send-question").attr("disabled","disabled");  //На всякий пожарный
                            } else{
                                $formMsg.addClass('is-modal-msg-visible').find('span').text(data.status+":"+data.Error);
                            }

                        } else {
                            $formMsg.addClass('is-modal-msg-visible').find('span').text('Извините произошла неизвестная ошибка :(');
                        }
                    }

                });
        }

        /*Форматирование цен*/
        function priceFormat(str){
            var s = str.toString();
            var l = str.toString().length;
            var price;
            switch (l) {
                case 5:
                    price = s.slice(0, 2) + '&#8198;' + s.slice(2);
                    break;
                case 6:
                    price = s.slice(0, 3) + '&#8198;' + s.slice(3);
                    break;
                default:
                    price = s;
            }
            return price;
        }

        /*Нажатие на верхнюю кнопку регистрации*/
        $('.js-link-reg').on('click', function(){
            ga('send', 'event', 'registration', 'top_action');
        });
        /*Нажатие на первую синяя кнопку регистрации*/
        $('#btnHome').on('click', function(){
            ga('send', 'event', 'registration', 'mid_action');
        });

        /*Нажатие на верхнюю кнопку войти*/
        $('.js-link-sign').on('click', function(){
            ga('send', 'event', 'login', 'top_action');
        });

        /*Метрики 90 секунд*/
        $('body').activity({
            'achieveTime':90
            ,'testPeriod':10
            ,useMultiMode: 1
            ,callBack: function (e) {
                /*Метрики*/
                ga('send', 'event', '60_sec', '60_sec');
            }
        });

        /*Sourcebuster*/
        sbjs.init({});

        $('input[placeholder]').placeholder();

    });

})();
