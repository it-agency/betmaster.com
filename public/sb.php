<?php

class SourceBuster {

    private $first_visit;
    private $current_visit;
    private $code;
    private $udata;
    public $allData;

    public function __construct() {
        $this->first_visit = $this->getData($_COOKIE['sbjs_first'], 'first_') + $this->getData($_COOKIE['sbjs_first_add'], 'first_');
        $this->current_visit = $this->getData($_COOKIE['sbjs_current'], 'current_') + $this->getData($_COOKIE['sbjs_current_add'], 'current_');
        $this->code = $this->getData($_COOKIE['sbjs_promo'], "");
        $this->udata = $this->getData($_COOKIE['sbjs_udata'], "");
        $this->allData = $this->first_visit + $this->current_visit + $this->code + $this->udata;
    }

    public function get($data, $var) {
        $array = $this->$data;
        return $array[$var];
    }

    public function getData($string, $prefix) {
        $ext_array = explode('|||',$string);
        $out_array = array();

        foreach($ext_array as $item)
        {
            $keyvalue = explode('=',$item);
            $key = $prefix. $keyvalue[0];
            unset($keyvalue[0]);
            $out_array[$key] = urldecode(implode('=',$keyvalue));
        }

        return $out_array;
    }
}

?>
